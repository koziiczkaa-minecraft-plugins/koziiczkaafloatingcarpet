# koziiczkaaFloatingCarpet

The plugin allows players to use their floating carpets.

## Config

In the config you can define the size and block the floating carpet is made of. You can also change all the messages
that are being sent either to the player or console.

An example config is present as a default config.

## Permissions

- `floatingcarpet.use` - allows player to turn their carpet on and off
- `floatingcarpet.use.other` - allows turning carpet of any player on and off
- `floatingcarpet.secure` - prevents from changing player's carpet's state by other players

## Commands

The main command is `/floatingcarpet` (alias `/fc`).

- `/floatingcarpet on` - turns the floating carpet **on**
- `/floatingcarpet on player` - turns the floating carpet **on** for specified `player`
- `/floatingcarpet off` - turns the floating carpet **off**
- `/floatingcarpet off player` - turns the floating carpet **off** for specified `player`
