/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import com.alicjakozik.spigot.floatingcarpet.entity.FloatingCarpetPlayer;
import lombok.AllArgsConstructor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
public class JsonManager {
	private static final String KEY_UUID = "uuid";
	private static final String KEY_SIZE = "size";
	private static final String KEY_MATERIAL = "material";
	private static final String KEY_ENABLED = "enabled";

	@NotNull
	private final FloatingCarpetPlugin plugin;

	/**
	 * Maps the json array to the players map.
	 *
	 * @param players json array read from the file.
	 * @return map of players.
	 */
	@NotNull
	public Map<OfflinePlayer, FloatingCarpetPlayer> jsonToMap(@NotNull final JSONArray players) {
		final Map<OfflinePlayer, FloatingCarpetPlayer> playersMap = new HashMap<>();
		players.forEach(object -> {
			final JSONObject jsonObject = (JSONObject) object;
			final UUID uuid = java.util.UUID.fromString(jsonObject.getString(KEY_UUID));
			final OfflinePlayer player = plugin.getServer().getOfflinePlayer(uuid);
			final int size = jsonObject.getInt(KEY_SIZE);
			final Material material = Material.valueOf(jsonObject.getString(KEY_MATERIAL));
			final boolean enabled = jsonObject.getBoolean(KEY_ENABLED);
			final FloatingCarpetPlayer floatingCarpetPlayer = new FloatingCarpetPlayer(player, size, material,
					enabled);
			playersMap.put(player, floatingCarpetPlayer);
		});
		return playersMap;
	}

	/**
	 * Maps the players map to the json array.
	 *
	 * @param players json array read from the file.
	 * @return json array containing players.
	 */
	@NotNull
	public JSONArray mapToJson(@NotNull final Map<OfflinePlayer, FloatingCarpetPlayer> players) {
		final JSONArray jsonArray = new JSONArray();
		players.values()
				.stream()
				.map(floatingCarpetPlayer -> new JSONObject()
						.put(KEY_UUID, floatingCarpetPlayer.getPlayer().getUniqueId().toString())
						.put(KEY_SIZE, floatingCarpetPlayer.getSize())
						.put(KEY_MATERIAL, floatingCarpetPlayer.getMaterial().name())
						.put(KEY_ENABLED, floatingCarpetPlayer.isEnabled()))
				.forEach(jsonArray::put);
		return jsonArray;
	}
}
