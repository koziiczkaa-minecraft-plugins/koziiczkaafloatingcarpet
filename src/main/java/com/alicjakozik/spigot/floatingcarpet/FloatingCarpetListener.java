/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jetbrains.annotations.NotNull;

@Value
@Getter(AccessLevel.NONE)
public class FloatingCarpetListener implements Listener {
	@NotNull
	FloatingCarpetPlugin plugin;

	@EventHandler
	public void onPlayerMoves(@NotNull final PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if (!plugin.isCarpetVisible(player)) {
			return;
		}
		final Location from = event.getFrom();
		plugin.hideCarpet(player, from);
		if (player.isSneaking()) {
			plugin.showCarpet(player, from.clone().add(0, -1, 0));
		} else {
			final Location to = event.getTo();
			if (to != null) {
				plugin.showCarpet(player, to);
			}
		}
	}
}
