/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.annotation.TypeFallback;
import eu.andret.arguments.api.entity.ExecutorType;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@BaseCommand("floatingcarpet")
public class FloatingCarpetCommand extends AnnotatedCommandExecutor<FloatingCarpetPlugin> {
	private static final String PLAYER = "${PLAYER}";
	private static final String SIZE = "${SIZE}";
	private static final String SECURE_PERMISSION = "floatingcarpet.secure";
	private static final String PLAYER_SECURED = "player-secured";

	public FloatingCarpetCommand(@NotNull final CommandSender sender, @NotNull final FloatingCarpetPlugin plugin) {
		super(sender, plugin);
	}

	@NotNull
	@TypeFallback(Player.class)
	public String playerFallback(final String player) {
		return plugin.getMessage("invalid-player").replace(PLAYER, player);
	}

	@Argument(permission = "floatingcarpet.use", executorType = ExecutorType.PLAYER)
	public String on() {
		final Player player = (Player) sender;
		if (!plugin.isCarpetVisible(player)) {
			plugin.showCarpet(player, player.getLocation());
			return plugin.getMessage("carpet-on");
		}
		return plugin.getMessage("already-on");
	}

	@NotNull
	@Argument(permission = "floatingcarpet.use.other")
	public String on(@NotNull final Player player) {
		if (player.hasPermission(SECURE_PERMISSION)) {
			return plugin.getMessage(PLAYER_SECURED).replace(PLAYER, player.getDisplayName());
		}
		if (!plugin.isCarpetVisible(player)) {
			plugin.showCarpet(player, player.getLocation());
			player.sendMessage(plugin.getMessage("carpet-on"));
			return plugin.getMessage("carpet-on-other").replace(PLAYER, player.getDisplayName());
		}
		return plugin.getMessage("already-on");
	}

	@Argument(permission = "floatingcarpet.use", executorType = ExecutorType.PLAYER)
	public String off() {
		final Player player = (Player) sender;
		if (plugin.isCarpetVisible(player)) {
			plugin.hideCarpet(player, player.getLocation());
			return plugin.getMessage("carpet-off");
		}
		return plugin.getMessage("already-off");
	}

	@NotNull
	@Argument(permission = "floatingcarpet.use.other")
	public String off(@NotNull final Player player) {
		if (player.hasPermission(SECURE_PERMISSION)) {
			return plugin.getMessage(PLAYER_SECURED).replace(PLAYER, player.getDisplayName());
		}
		if (plugin.isCarpetVisible(player)) {
			plugin.hideCarpet(player, player.getLocation());
			player.sendMessage(plugin.getMessage("carpet-off"));
			return plugin.getMessage("carpet-off-other").replace(PLAYER, player.getDisplayName());
		}
		return plugin.getMessage("already-off");
	}

	@NotNull
	@Argument(permission = "floatingcarpet.size")
	public String size(final int size) {
		final Player player = (Player) sender;
		final boolean carpetVisible = plugin.isCarpetVisible(player);
		if (carpetVisible) {
			plugin.hideCarpet(player, player.getLocation());
		}
		if (!plugin.setSize(player, size)) {
			return plugin.getMessage("size-failure")
					.replace(SIZE, String.valueOf(plugin.getConfig().getInt("floatingcarpet.max-size")));
		}
		if (carpetVisible) {
			plugin.showCarpet(player, player.getLocation());
		}
		return plugin.getMessage("size-success").replace(PLAYER, player.getDisplayName());
	}

	@NotNull
	@Argument(permission = "floatingcarpet.size.other")
	public String size(final int size, @NotNull final Player player) {
		if (player.hasPermission(SECURE_PERMISSION)) {
			return plugin.getMessage(PLAYER_SECURED).replace(PLAYER, player.getDisplayName());
		}
		final boolean carpetVisible = plugin.isCarpetVisible(player);
		if (carpetVisible) {
			plugin.hideCarpet(player, player.getLocation());
		}
		if (!plugin.setSize(player, size)) {
			return plugin.getMessage("size-failure")
					.replace(SIZE, String.valueOf(plugin.getConfig().getInt("floatingcarpet.max-size")));
		}
		if (carpetVisible) {
			plugin.showCarpet(player, player.getLocation());
		}
		player.sendMessage(plugin.getMessage("size-success"));
		return plugin.getMessage("size-success-other").replace(PLAYER, player.getDisplayName());
	}

	@NotNull
	@Argument(permission = "floatingcarpet.material")
	public String material(@NotNull final Material material) {
		final Player player = (Player) sender;
		final boolean carpetVisible = plugin.isCarpetVisible(player);
		if (carpetVisible) {
			plugin.hideCarpet(player, player.getLocation());
		}
		if (!plugin.setMaterial(player, material)) {
			return plugin.getMessage("material-failure");
		}
		if (carpetVisible) {
			plugin.showCarpet(player, player.getLocation());
		}
		return plugin.getMessage("material-success").replace(PLAYER, player.getDisplayName());
	}

	@NotNull
	@Argument(permission = "floatingcarpet.material.other")
	public String material(@NotNull final Material material, @NotNull final Player player) {
		if (player.hasPermission(SECURE_PERMISSION)) {
			return plugin.getMessage(PLAYER_SECURED).replace(PLAYER, player.getDisplayName());
		}
		final boolean carpetVisible = plugin.isCarpetVisible(player);
		if (carpetVisible) {
			plugin.hideCarpet(player, player.getLocation());
		}
		if (!plugin.setMaterial(player, material)) {
			return plugin.getMessage("material-failure");
		}
		if (carpetVisible) {
			plugin.showCarpet(player, player.getLocation());
		}
		player.sendMessage(plugin.getMessage("material-success"));
		return plugin.getMessage("material-success-other").replace(PLAYER, player.getDisplayName());
	}
}
