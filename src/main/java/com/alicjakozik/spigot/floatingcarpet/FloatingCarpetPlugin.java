/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import com.alicjakozik.spigot.floatingcarpet.entity.FloatingCarpetPlayer;
import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.arguments.api.entity.FallbackConstants;
import org.bstats.bukkit.Metrics;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class FloatingCarpetPlugin extends JavaPlugin {
	private static final String FLOATING_CARPET = "FloatingCarpet";
	private final File carpetsDataFile = new File(getDataFolder(), "carpets-list.json");
	private final Map<OfflinePlayer, FloatingCarpetPlayer> floatingCarpetPlayers = new HashMap<>();
	private int carpetSize;
	private int carpetMaxSize;
	private Material carpetMaterial;
	private final JsonManager jsonManager = new JsonManager(this);


	@Override
	public void onEnable() {
		saveDefaultConfig();
		readJson();
		final AnnotatedCommand<FloatingCarpetPlugin> annotatedCommand = CommandManager
				.registerCommand(FloatingCarpetCommand.class, this);
		annotatedCommand.setOnMainCommandExecutionListener(commandSender ->
				commandSender.sendMessage(getMessage("argument-error")));
		annotatedCommand.setOnInsufficientPermissionsListener(commandSender ->
				commandSender.sendMessage(getMessage("access-denied")));
		annotatedCommand.setOnUnknownSubCommandExecutionListener(commandSender ->
				commandSender.sendMessage(getMessage("wrong-argument")));
		annotatedCommand.addTypeMapper(Player.class, s -> getServer().getPlayer(s), FallbackConstants.ON_NULL);
		annotatedCommand.addTypeMapper(Material.class, Material::getMaterial, FallbackConstants.ON_NULL);
		annotatedCommand.addTypeCompleter(Player.class, (commandSender, strings) ->
				getServer().getOnlinePlayers()
						.stream()
						.map(Player::getDisplayName)
						.filter(name -> name.startsWith(new ArrayList<>(strings).get(strings.size() - 1)))
						.toList());
		annotatedCommand.addTypeCompleter(Material.class, (commandSender, strings) -> Arrays.stream(Material.values())
				.filter(material -> material.isSolid() && material.isBlock() && !material.hasGravity()
						&& !material.isInteractable())
				.map(String::valueOf)
				.filter(name -> name.contains(new ArrayList<>(strings).get(strings.size() - 1)))
				.toList());

		getServer().getPluginManager().registerEvents(new FloatingCarpetListener(this), this);
		carpetMaterial = Material.valueOf(getConfig().getString("floatingcarpet.block", "glass")
				.toUpperCase());
		carpetSize = getConfig().getInt("floatingcarpet.size");
		carpetMaxSize = getConfig().getInt("floatingcarpet.max-size");
		new Metrics(this, 16370);
	}

	/**
	 * Reads the JSON file
	 */
	private void readJson() {
		if (carpetsDataFile.exists()) {
			try {
				final FileInputStream inputStream = new FileInputStream(carpetsDataFile);
				final JSONTokener jsonTokener = new JSONTokener(inputStream);
				final JSONArray players = new JSONArray(jsonTokener);
				floatingCarpetPlayers.putAll(jsonManager.jsonToMap(players));
			} catch (final FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDisable() {
		try (final PrintWriter printWriter = new PrintWriter(new FileWriter(carpetsDataFile))) {
			printWriter.println(jsonManager.mapToJson(floatingCarpetPlayers).toString(4));
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the config message of a specific key.
	 *
	 * @param configProperty The config key.
	 * @return The message found in config.
	 * @throws IllegalArgumentException When the key not found in the config.
	 */
	@NotNull
	public String getMessage(@NotNull final String configProperty) {
		final ConfigurationSection messages = getConfig().getConfigurationSection("messages");
		if (messages == null) {
			throw new IllegalArgumentException("Messages configuration section doesn't exist");
		}
		return ChatColor.translateAlternateColorCodes('&', messages.getString(configProperty, ""));
	}

	/**
	 * Creates the carpet at the provided location.
	 *
	 * @param player   The player to show the carpet for.
	 * @param location The location to create the carpet at.
	 */
	public void showCarpet(@NotNull final Player player, @NotNull final Location location) {
		final FloatingCarpetPlayer floatingCarpetPlayer = getFloatingCarpetPlayer(player);
		if (floatingCarpetPlayer.isEnabled()) {
			return;
		}
		final int size = floatingCarpetPlayer.getSize();
		for (int x = -size; x <= size; x++) {
			for (int z = -size; z <= size; z++) {
				final Block block = location.getBlock().getRelative(x, -1, z);
				if (block.getType() == Material.AIR) {
					final Material material = floatingCarpetPlayer.getMaterial();
					block.setType(material);
					block.setMetadata(FLOATING_CARPET, new FixedMetadataValue(this, player.getUniqueId()));
				}
			}
		}
		floatingCarpetPlayer.setEnabled(true);
	}

	/**
	 * Destroys the carpet at the provided location.
	 *
	 * @param player   The player to hide the carpet for.
	 * @param location The location to create the carpet at.
	 */
	public void hideCarpet(@NotNull final Player player, @NotNull final Location location) {
		final FloatingCarpetPlayer floatingCarpetPlayer = getFloatingCarpetPlayer(player);
		if (!floatingCarpetPlayer.isEnabled()) {
			return;
		}
		final int size = floatingCarpetPlayer.getSize();
		for (int x = -size - 1; x <= size + 1; x++) {
			for (int y = -2; y <= 0; y++) {
				for (int z = -size - 1; z <= size + 1; z++) {
					final Block block = location.getBlock().getRelative(x, y, z);
					final List<MetadataValue> blockMetadata = block.getMetadata(FLOATING_CARPET);
					if (blockMetadata.isEmpty()) {
						continue;
					}
					final String metaDataValue = blockMetadata.get(0).asString();
					if (metaDataValue.equals(floatingCarpetPlayer.getPlayer().getUniqueId().toString())) {
						block.setType(Material.AIR);
					}
				}
			}
		}
		floatingCarpetPlayer.setEnabled(false);
	}

	/**
	 * Checks if the carpet is visible for the provided player.
	 *
	 * @param player The player to check if the carpet is enabled for.
	 * @return {@code true} if the carpet is visible, {@code false} otherwise.
	 */
	public boolean isCarpetVisible(@NotNull final Player player) {
		return getFloatingCarpetPlayer(player).isEnabled();
	}

	/**
	 * Sets the size of the carpet.
	 *
	 * @param player The player for which we want to change carpet's size.
	 * @param size   The new size of the carpet.
	 * @return {@code true} if carpet's size has been set successfully, {@code false} otherwise.
	 */
	public boolean setSize(@NotNull final Player player, final int size) {
		if (size >= 1 && size <= carpetMaxSize) {
			getFloatingCarpetPlayer(player).setSize(size);
			return true;
		}
		return false;
	}

	/**
	 * Sets the material of the carpet.
	 *
	 * @param player   The player for which we want to change carpet's material.
	 * @param material The new carpet's material.
	 * @return {@code true} if carpet's material has been set successfully, {@code false} otherwise.
	 */
	public boolean setMaterial(@NotNull final Player player, @NotNull final Material material) {
		if (material.isSolid() && material.isBlock() && !material.isInteractable() && !material.hasGravity()) {
			getFloatingCarpetPlayer(player).setMaterial(material);
			return true;
		}
		return false;
	}

	/**
	 * Gets or creates a floating carpet player.
	 *
	 * @param player Found or created player.
	 * @return floating carpet player.
	 */
	@NotNull
	private FloatingCarpetPlayer getFloatingCarpetPlayer(@NotNull final OfflinePlayer player) {
		final FloatingCarpetPlayer floatingCarpetPlayer = getPlayerFromMap(player);
		if (floatingCarpetPlayer != null) {
			return floatingCarpetPlayer;
		}
		final FloatingCarpetPlayer carpetPlayer = new FloatingCarpetPlayer(player, carpetSize, carpetMaterial, false);
		floatingCarpetPlayers.put(player, carpetPlayer);
		return carpetPlayer;
	}

	@Nullable
	private FloatingCarpetPlayer getPlayerFromMap(@NotNull final OfflinePlayer player) {
		return floatingCarpetPlayers.entrySet()
				.stream()
				.filter(entry -> entry.getKey().getUniqueId().equals(player.getUniqueId()))
				.map(Map.Entry::getValue)
				.findFirst()
				.orElse(null);
	}
}
