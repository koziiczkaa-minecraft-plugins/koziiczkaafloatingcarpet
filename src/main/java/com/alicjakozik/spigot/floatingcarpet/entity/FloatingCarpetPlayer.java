/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public class FloatingCarpetPlayer {
	@NotNull
	private final OfflinePlayer player;
	private int size;
	@NotNull
	private Material material;
	private boolean enabled;
}
