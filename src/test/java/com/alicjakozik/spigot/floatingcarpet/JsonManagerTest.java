/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import com.alicjakozik.spigot.floatingcarpet.entity.FloatingCarpetPlayer;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class JsonManagerTest {
	@Test
	void mappingNonEmptyJsonArray() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Server server = mock(Server.class);
		final OfflinePlayer offlinePlayer = mock(OfflinePlayer.class);
		when(plugin.getServer()).thenReturn(server);
		when(server.getOfflinePlayer(any(UUID.class))).thenReturn(offlinePlayer);
		final FloatingCarpetPlayer floatingCarpetPlayer = new FloatingCarpetPlayer(offlinePlayer, 5,
				Material.ACACIA_PLANKS, true);
		final JsonManager jsonManager = new JsonManager(plugin);
		final JSONArray jsonArray = new JSONArray();
		final JSONObject jsonObject = new JSONObject()
				.put("uuid", "7bb7d2b3-89e0-4d54-927c-2ca0c9d9c307")
				.put("size", 5)
				.put("material", "ACACIA_PLANKS")
				.put("enabled", true);
		jsonArray.put(jsonObject);

		// when
		final Map<OfflinePlayer, FloatingCarpetPlayer> result = jsonManager.jsonToMap(jsonArray);

		// then
		assertEquals(1, result.size());
		assertEquals(floatingCarpetPlayer, result.get(offlinePlayer));
	}

	@Test
	void mappingEmptyJsonArray() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final JsonManager jsonManager = new JsonManager(plugin);
		final JSONArray jsonArray = new JSONArray();

		// when
		final Map<OfflinePlayer, FloatingCarpetPlayer> result = jsonManager.jsonToMap(jsonArray);

		// then
		assertTrue(result.isEmpty());
	}

	@Test
	void mappingNonEmptyMapToJson() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final JsonManager jsonManager = new JsonManager(plugin);
		final OfflinePlayer offlinePlayer = mock(OfflinePlayer.class);
		final UUID uuid = UUID.fromString("7bb7d2b3-89e0-4d54-927c-2ca0c9d9c307");
		when(offlinePlayer.getUniqueId()).thenReturn(uuid);
		final FloatingCarpetPlayer floatingCarpetPlayer = new FloatingCarpetPlayer(offlinePlayer, 5,
				Material.ACACIA_PLANKS, true);
		final Map<OfflinePlayer, FloatingCarpetPlayer> players = new HashMap<>();
		players.put(offlinePlayer, floatingCarpetPlayer);
		final JSONArray jsonArray = new JSONArray();
		final JSONObject jsonObject = new JSONObject()
				.put("uuid", "7bb7d2b3-89e0-4d54-927c-2ca0c9d9c307")
				.put("size", 5)
				.put("material", "ACACIA_PLANKS")
				.put("enabled", true);
		jsonArray.put(jsonObject);

		// when
		final JSONArray result = jsonManager.mapToJson(players);

		// then
		JSONAssert.assertEquals(jsonArray, result, true);
	}

	@Test
	void mappingEmptyMapToJson() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final JsonManager jsonManager = new JsonManager(plugin);
		final Map<OfflinePlayer, FloatingCarpetPlayer> players = new HashMap<>();

		// when
		final JSONArray result = jsonManager.mapToJson(players);

		// then
		JSONAssert.assertEquals(new JSONArray(), result, true);
	}
}
