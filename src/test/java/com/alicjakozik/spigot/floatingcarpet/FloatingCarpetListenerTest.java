/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class FloatingCarpetListenerTest {
	@Test
	void playerMovesWithNoCarpet() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final PlayerMoveEvent moveEvent = mock(PlayerMoveEvent.class);
		final Player player = mock(Player.class);
		final FloatingCarpetListener listener = new FloatingCarpetListener(plugin);
		when(plugin.isCarpetVisible(any(Player.class))).thenReturn(false);
		when(moveEvent.getPlayer()).thenReturn(player);

		// when
		listener.onPlayerMoves(moveEvent);

		// then
		verify(plugin, times(0)).showCarpet(any(Player.class), any(Location.class));
		verify(plugin, times(0)).hideCarpet(any(Player.class), any(Location.class));
	}

	@Test
	void playerMovesNotSneaking() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final PlayerMoveEvent moveEvent = mock(PlayerMoveEvent.class);
		final Player player = mock(Player.class);
		final World world = mock(World.class);
		final FloatingCarpetListener listener = new FloatingCarpetListener(plugin);
		final Location locationFrom = new Location(world, 1, 2, 3);
		final Location locationTo = new Location(world, 2, 3, 4);
		when(plugin.isCarpetVisible(any(Player.class))).thenReturn(true);
		when(moveEvent.getPlayer()).thenReturn(player);
		when(moveEvent.getFrom()).thenReturn(locationFrom);
		when(moveEvent.getTo()).thenReturn(locationTo);
		when(player.isSneaking()).thenReturn(false);

		// when
		listener.onPlayerMoves(moveEvent);

		// then
		verify(plugin, times(1)).showCarpet(player, locationTo);
		verify(plugin, times(1)).hideCarpet(player, locationFrom);
	}

	@Test
	void playerMovesSneaking() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final PlayerMoveEvent moveEvent = mock(PlayerMoveEvent.class);
		final Player player = mock(Player.class);
		final World world = mock(World.class);
		final FloatingCarpetListener listener = new FloatingCarpetListener(plugin);
		final Location locationFrom = new Location(world, 1, 2, 3);
		final Location targetLocation = new Location(world, 1, 1, 3);
		final Location locationTo = new Location(world, 2, 3, 4);
		when(plugin.isCarpetVisible(any(Player.class))).thenReturn(true);
		when(moveEvent.getPlayer()).thenReturn(player);
		when(moveEvent.getFrom()).thenReturn(locationFrom);
		when(moveEvent.getTo()).thenReturn(locationTo);
		when(player.isSneaking()).thenReturn(true);

		// when
		listener.onPlayerMoves(moveEvent);

		// then
		verify(plugin, times(1)).showCarpet(player, targetLocation);
		verify(plugin, times(1)).hideCarpet(player, locationFrom);
	}

	@Test
	void playerMovesMalformed() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final PlayerMoveEvent moveEvent = mock(PlayerMoveEvent.class);
		final Player player = mock(Player.class);
		final World world = mock(World.class);
		final FloatingCarpetListener listener = new FloatingCarpetListener(plugin);
		final Location locationFrom = new Location(world, 1, 2, 3);
		when(plugin.isCarpetVisible(any(Player.class))).thenReturn(true);
		when(moveEvent.getPlayer()).thenReturn(player);
		when(moveEvent.getFrom()).thenReturn(locationFrom);
		when(moveEvent.getTo()).thenReturn(null);
		when(player.isSneaking()).thenReturn(false);

		// when
		listener.onPlayerMoves(moveEvent);

		// then
		verify(plugin, times(0)).showCarpet(any(Player.class), any(Location.class));
		verify(plugin, times(1)).hideCarpet(player, locationFrom);
	}
}
