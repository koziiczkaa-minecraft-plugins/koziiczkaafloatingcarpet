/*
 * Copyright koziiczkaa (c) 2022. Copying and modifying allowed only by keeping the git link reference.
 *
 */

package com.alicjakozik.spigot.floatingcarpet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class FloatingCarpetCommandTest {
	@Test
	void fallback() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(plugin.getMessage("invalid-player")).thenReturn("I'm ${PLAYER}");

		// when
		final String result = command.playerFallback("test");

		// then
		assertEquals("I'm test", result);
	}

	@Test
	void carpetOnWithoutPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		when(player.getLocation()).thenReturn(location);
		when(plugin.isCarpetVisible(player)).thenReturn(false);
		when(plugin.getMessage("carpet-on")).thenReturn("Carpet on");

		// when
		final String result = command.on();

		// then
		verify(plugin, times(1)).showCarpet(player, location);
		assertEquals("Carpet on", result);
	}

	@Test
	void carpetAlreadyOnWithoutPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		when(player.getLocation()).thenReturn(location);
		when(plugin.isCarpetVisible(player)).thenReturn(true);
		when(plugin.getMessage("already-on")).thenReturn("Carpet already on");

		// when
		final String result = command.on();

		// then
		assertEquals("Carpet already on", result);
	}

	@Test
	void carpetOffWithoutPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		when(player.getLocation()).thenReturn(location);
		when(plugin.isCarpetVisible(player)).thenReturn(true);
		when(plugin.getMessage("carpet-off")).thenReturn("Carpet off");

		// when
		final String result = command.off();

		// then
		verify(plugin, times(1)).hideCarpet(player, location);
		assertEquals("Carpet off", result);
	}

	@Test
	void carpetAlreadyOffWithoutPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		when(player.getLocation()).thenReturn(location);
		when(plugin.isCarpetVisible(player)).thenReturn(false);
		when(plugin.getMessage("already-off")).thenReturn("Carpet already off");

		// when
		final String result = command.off();

		// then
		assertEquals("Carpet already off", result);
	}

	@Test
	void carpetOnWithSecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("player-secured")).thenReturn("Player ${PLAYER} is secured");

		// when
		final String result = command.on(player);

		// then
		assertEquals("Player test is secured", result);
	}

	@Test
	void carpetOnWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(player.getLocation()).thenReturn(location);
		when(plugin.getMessage("carpet-on")).thenReturn("Carpet is on");
		when(plugin.getMessage("carpet-on-other")).thenReturn("${PLAYER}'s carpet is on");
		when(plugin.isCarpetVisible(player)).thenReturn(false);

		// when
		final String result = command.on(player);

		// then
		verify(plugin, times(1)).showCarpet(player, location);
		verify(player, times(1)).sendMessage("Carpet is on");
		assertEquals("test's carpet is on", result);
	}

	@Test
	void carpetAlreadyOnWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(plugin.getMessage("already-on")).thenReturn("Carpet is already on");
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.on(player);

		// then
		assertEquals("Carpet is already on", result);
	}

	@Test
	void carpetOffWithSecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("player-secured")).thenReturn("Player ${PLAYER} is secured");

		// when
		final String result = command.off(player);

		// then
		assertEquals("Player test is secured", result);
	}

	@Test
	void carpetOffWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(player.getLocation()).thenReturn(location);
		when(plugin.getMessage("carpet-off")).thenReturn("Carpet is off");
		when(plugin.getMessage("carpet-off-other")).thenReturn("${PLAYER}'s carpet is off");
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.off(player);

		// then
		verify(plugin, times(1)).hideCarpet(player, location);
		verify(player, times(1)).sendMessage("Carpet is off");
		assertEquals("test's carpet is off", result);
	}

	@Test
	void carpetAlreadyOffWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(plugin.getMessage("already-off")).thenReturn("Carpet is already off");
		when(plugin.isCarpetVisible(player)).thenReturn(false);

		// when
		final String result = command.off(player);

		// then
		assertEquals("Carpet is already off", result);
	}

	@Test
	void carpetSizeWithoutPlayerSuccess() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final int size = 5;
		when(plugin.setSize(player, size)).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("size-success")).thenReturn("Size has been set");

		// when
		final String result = command.size(size);

		// then
		verify(plugin, times(1)).setSize(player, size);
		assertEquals("Size has been set", result);
	}

	@Test
	void carpetSizeWithoutPlayerFailure() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final FileConfiguration config = mock(FileConfiguration.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final int size = 5;
		when(plugin.getConfig()).thenReturn(config);
		when(config.getInt("floatingcarpet.max-size")).thenReturn(5);
		when(plugin.setSize(player, size)).thenReturn(false);
		when(plugin.getMessage("size-failure")).thenReturn("Size change failure");

		// when
		final String result = command.size(size);

		// then
		verify(plugin, times(1)).setSize(player, size);
		assertEquals("Size change failure", result);
	}

	@Test
	void carpetSizeWithSecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("player-secured")).thenReturn("Player ${PLAYER} is secured");

		// when
		final String result = command.size(3, player);

		// then
		assertEquals("Player test is secured", result);
	}

	@Test
	void carpetSizeWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		final int size = 3;
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("size-success")).thenReturn("Size has been changed");
		when(plugin.getMessage("size-success-other"))
				.thenReturn("Size of ${PLAYER}'s carpet has been changed");
		when(plugin.setSize(any(Player.class), anyInt())).thenReturn(true);

		// when
		final String result = command.size(size, player);

		// then
		verify(plugin, times(1)).setSize(player, size);
		verify(player, times(1)).sendMessage("Size has been changed");
		assertEquals("Size of test's carpet has been changed", result);
	}

	@Test
	void carpetSizeWithPlayerFailure() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final FileConfiguration config = mock(FileConfiguration.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		final int size = 6;
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(plugin.getConfig()).thenReturn(config);
		when(config.getInt("floatingcarpet.max-size")).thenReturn(5);
		when(plugin.getMessage("size-failure")).thenReturn("Changing size failure");
		when(plugin.setSize(any(Player.class), anyInt())).thenReturn(false);

		// when
		final String result = command.size(size, player);

		// then
		verify(plugin, times(1)).setSize(player, size);
		assertEquals("Changing size failure", result);
	}

	@Test
	void carpetSizeWithoutPlayerAndCarpetOn() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final int size = 5;
		when(plugin.setSize(player, size)).thenReturn(true);
		when(player.getLocation()).thenReturn(location);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("size-success")).thenReturn("Size has been set");
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.size(size);

		// then
		verify(plugin, times(1)).setSize(player, size);
		verify(plugin, times(1)).hideCarpet(player, location);
		verify(plugin, times(1)).showCarpet(player, location);
		verify(plugin, times(1)).isCarpetVisible(player);
		assertEquals("Size has been set", result);
	}

	@Test
	void carpetSizeWithUnsecuredPlayerAndCarpetOn() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		final int size = 5;
		when(player.getLocation()).thenReturn(location);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("size-success")).thenReturn("Size has been changed");
		when(plugin.getMessage("size-success-other"))
				.thenReturn("Size of ${PLAYER}'s carpet has been changed");
		when(plugin.setSize(any(Player.class), anyInt())).thenReturn(true);
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.size(size, player);

		// then
		verify(plugin, times(1)).setSize(player, size);
		verify(plugin, times(1)).hideCarpet(player, location);
		verify(plugin, times(1)).showCarpet(player, location);
		verify(plugin, times(1)).isCarpetVisible(player);
		verify(player, times(1)).sendMessage("Size has been changed");
		assertEquals("Size of test's carpet has been changed", result);
	}

	@Test
	void carpetMaterialWithoutPlayerSuccess() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final Material material = Material.GLASS;
		when(plugin.setMaterial(player, material)).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("material-success")).thenReturn("Material for ${PLAYER} has been set");

		// when
		final String result = command.material(material);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		assertEquals("Material for test has been set", result);
	}

	@Test
	void carpetMaterialWithoutPlayerFailure() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final Material material = Material.STONE;
		when(plugin.setMaterial(player, material)).thenReturn(false);
		when(plugin.getMessage("material-failure")).thenReturn("Material change failure");

		// when
		final String result = command.material(material);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		assertEquals("Material change failure", result);
	}

	@Test
	void carpetMaterialWithSecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("player-secured")).thenReturn("Player ${PLAYER} is secured");

		// when
		final String result = command.material(Material.GLASS, player);

		// then
		assertEquals("Player test is secured", result);
	}

	@Test
	void carpetMaterialWithUnsecuredPlayer() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		final Material material = Material.GLASS;
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("material-success")).thenReturn("Material has been changed");
		when(plugin.getMessage("material-success-other"))
				.thenReturn("Material of ${PLAYER} has been changed");
		when(plugin.setMaterial(any(Player.class), any(Material.class))).thenReturn(true);

		// when
		final String result = command.material(material, player);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		verify(player, times(1)).sendMessage("Material has been changed");
		assertEquals("Material of test has been changed", result);
	}

	@Test
	void carpetMaterialWithPlayerFailure() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final Material material = Material.STONE;
		when(plugin.setMaterial(player, material)).thenReturn(false);
		when(plugin.getMessage("material-failure")).thenReturn("Material change failure");

		// when
		final String result = command.material(material, player);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		assertEquals("Material change failure", result);
	}

	@Test
	void carpetMaterialWithoutPlayerAndCarpetOn() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(player, plugin);
		final Material material = Material.GLASS;
		when(player.getLocation()).thenReturn(location);
		when(plugin.setMaterial(player, material)).thenReturn(true);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("material-success")).thenReturn("Material for ${PLAYER} has been set");
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.material(material);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		verify(plugin, times(1)).hideCarpet(player, location);
		verify(plugin, times(1)).showCarpet(player, location);
		verify(plugin, times(1)).isCarpetVisible(player);
		assertEquals("Material for test has been set", result);
	}

	@Test
	void carpetMaterialWithUnsecuredPlayerAndCarpetOn() {
		// given
		final FloatingCarpetPlugin plugin = mock(FloatingCarpetPlugin.class);
		final CommandSender sender = mock(CommandSender.class);
		final Player player = mock(Player.class);
		final Location location = mock(Location.class);
		final FloatingCarpetCommand command = new FloatingCarpetCommand(sender, plugin);
		final Material material = Material.GLASS;
		when(player.getLocation()).thenReturn(location);
		when(player.hasPermission("floatingcarpet.secure")).thenReturn(false);
		when(player.getDisplayName()).thenReturn("test");
		when(plugin.getMessage("material-success")).thenReturn("Material has been changed");
		when(plugin.getMessage("material-success-other"))
				.thenReturn("Material of ${PLAYER} has been changed");
		when(plugin.setMaterial(any(Player.class), any(Material.class))).thenReturn(true);
		when(plugin.isCarpetVisible(player)).thenReturn(true);

		// when
		final String result = command.material(material, player);

		// then
		verify(plugin, times(1)).setMaterial(player, material);
		verify(plugin, times(1)).hideCarpet(player, location);
		verify(plugin, times(1)).showCarpet(player, location);
		verify(plugin, times(1)).isCarpetVisible(player);
		verify(player, times(1)).sendMessage("Material has been changed");
		assertEquals("Material of test has been changed", result);
	}
}
